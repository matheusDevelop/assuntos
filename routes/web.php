<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes(['register' => true]);


Route::get('/admin', 'AdminController@index')->name('admin');



Route::group(['middleware' => ['auth']], function () {

    Route::prefix('admin')->group(function () {

        Route::prefix('user')->group(function () {
            Route::post('new', 'UserController@store')->name('user-store');

            Route::get('editar/{id}', 'UserController@edit')->name('user-edit');
            Route::post('editar/{id}', 'UserController@update')->name('user-update');

            Route::post('excluir/{id}', 'UserController@delete')->name('user-delete');
        });

        Route::get('assuntos','SubjectController@index')->name('subjects');

        Route::prefix('assunto')->group(function () {

            Route::get('novo','SubjectController@create')->name('subject-create');
            Route::post('novo','SubjectController@store')->name('subject-store');

            Route::get('editar/{slug}','SubjectController@edit')->name('subject-edit');
            Route::post('editar/{slug}','SubjectController@update')->name('subject-update');

            Route::post('excluir/{slug}','SubjectController@destroy')->name('subject-delete');

            Route::post('search', 'SubjectController@search')->name('subject-search');
        });
    });
});
