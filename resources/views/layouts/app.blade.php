<html>
<head>
    <title>Verdes Mares - @yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/dropzone.js') }}"></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/intera-icons.css') }}" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('image/favicon.ico') }}">


</head>
<body>
<div id="app">
    <!-- Navbar -->
    <nav class="navbar navbar-custom navbar__styled">
        <a class="navbar-brand" href="{{route('subjects')}}">Oráculo</a>

        @auth

        <div class="app-search">
            <form>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search...">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">Pesquisar</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="topbar-right-menu mb-0">
            <div class="dropdown">
                <button class="btn btn-link nav-link dropdown-toggle nav-user arrow-none mr-0" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <span class="account-user-avatar"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBAQFBAYFBQYJBgUGCQsIBgYICwwKCgsKCgwQDAwMDAwMEAwODxAPDgwTExQUExMcGxsbHB8fHx8fHx8fHx//2wBDAQcHBw0MDRgQEBgaFREVGh8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx//wAARCACAAIADAREAAhEBAxEB/8QAHAAAAQUBAQEAAAAAAAAAAAAAAgEDBAUGAAcI/8QAOxAAAgEDAgQFAgUBBQkBAAAAAQIDAAQRBSESMUFRBhMiYXEHMhRSgZGhQiMzsbLRFRZiY3KCkqLB4f/EABoBAAIDAQEAAAAAAAAAAAAAAAECAAMEBQb/xAAlEQACAgEDBQADAQEAAAAAAAAAAQIRAwQSIQUTMUFRIjJhoXH/2gAMAwEAAhEDEQA/APQ+9cuzrCDagmEU8/fpRsgXtTAImpavp2mwNPezrBGu5Zv/AJinjFvwBs801r61eXcyxabbebbcOElkPCc9wB0+a2Q0nHIjK/TfrjqMZC6hYRzJ1eFijfs3EKeWjXpktml0j6yeGr24jt7mOayaQ4EknCYwT+ZgdqplpJJX5DZsk1TTJApju4XDnCcMinPXbBrM4P4SyUsisSFIOOeKDCOCgQUqCMEUCCcBJz1o0SwxtzqBDTcb9KKFYL/zUYUhoHJNVDgnORShOOc/FEASk8qZAZ5L9b9WMMVrZISrOWZiMcgNhXQ0keWxTxvz3OeI/dW9MU5ZSMn3o2QIzg5JO3UUbIEl4VxwkjB2IOMUCGk8NeO9e0edfIu3a3LBpIXPGD0/qz0qnJhjJeAo+hfDus22s6TBf2/FwSjfiUruNjsfeuVOO10Es8N3xSkOMjCpZKEBPyaARwDb550QAMMHegwgdaRoYTOTQCdk1KIEp23p0IzxH64aRqbaxFqLYNgIljQ53DZPFt+1dDSyVV7JXB5ckMkknBEpkbsozW2wVZKj0DV3OPw7jHPIxQ3IPbZGmtZ4GMcgwc4I96ZCtUN+Wf3qUAcRSu4Oe4qBR7z9GvFCX2jDSm2ubEeoY5oT6Wz/ABXO1MKd/QtHpBfI3FZGyCcSYxjegQ7iGdqIQuLK70QUN4y2BQCDnvVYxwcDpvQsjFyppkyCFx2pkCig8U6TbajFFHOvmxsSHhP2sBvv+tXY7vgMWUll4X0jSIG/DWaoMZZzgsf3rUrZHKyuuIrQlmCAZ/SnQTz3xhpKR5ljxxZLBe4NXwkLJGSBB9qsKxACDj96hD0X6Isf98GGcBrWTbvgrWTVr8RrPesZrmkAwc0A2EABUIFxDFFEEwM1AjBOeuKqCCQ2aUY7JFWRQrKvxB4k03QrRLm/Z+CR/LjSNeJmbGcDlV2ODk+BWYDVvqilzqiraQMtsi44W3cnBJb0cS4rZDT/ANJuoobbx3qFwZpJLpbdE4eHzCBniJwAD8VpjhRXkm/gF59QAYY04UndywkcDAwOXCRz99qbtRJFyK3UNXtdRtfJmiKMN43B3BPzijHF8Y+/6jLzafdqxcIWTPMb1ZtaE3IjnIPqG4pQnoX0RRm8ZlgMqlrKWPbJUVl1f6BPfQN65gbFIxUIhOZqBOzvUIdn1UAkcgAVXQRNqlEF2NWIBjvqdaRS6NaSOgYRXQ3I5ccbr/jWjTvki8mO0Xw1FYaJe6mylZ9RVre1Db8ERHqYZ/Of4ram2Nts8yubOaO5dJEJKEjl71oRU0XegaC9zhnXCg7AjnncmknIsgqNjFoFg1t5csYIqn2M5GVm0S4j15NOhmdbeY5ViM4XBOAT8VoWR0U7U2aDTvD+lm4a2/DxvLwnznm9ci9jwttg46VmeSTNEYJF/wDSrQPwXibVLqJOC3FvGigcg8jcRUE/9Of1qrUTuKQmWKT4PVAR+tYitisc1AoTPvQCcCaJA0GeY5VKA2Rj3zVYRA25qIJwYimQGNXVtbXULQ3MSzQv90bgMpx7GmToB5p48uPEem3sMdq0b2SkLDEVyAuMqvDtjat+CW5FqkqKPT7Jb29lvNSt0jEgAManJBH9Watkmgb0WRtYbbaHZenWlTsjYvnOR8UUhWxk3CxuHO+4C57scUX4FiuQ76XT9JgOpXLkzHibLY4yzD7BjOaVRsvUqXJu/p7YzW+gJd3Q4bzUm/FSr0VWGI0/7UAFZc0rlRnnK3Zp+E96oaAmKT3qDHDnUogTkZ2FFoCFBwMVEyEcjbFVDABTnaloaxeA47U4rYhXvRBZifHMUoukfzYIIWj/AL24QyevJGFA61r07HjT8mLTTdQmmDQakjhf6VjKgj/yNa7QWkOG8Ct5bnBXY9sihQKF/FqTty7020UW3eOaYKfUM8qE+ESJYXmhjWrrTtP5I86PKf8Alp6n/gVUpbU2GTPVFAQBVGFXZR2ArCVDoaoyC539+tRMISnfFQIRagQEHJ75oBGlGelKgnBcVKJYpHpxTIWxOEUSGd8awWhsIprqPjiic57Anln9qtw+SI8+v9U0qziaPT4s3En3N0ArfGLZLM8WmZ+N9snOKtoax6MyPsNhQsNFppkDrIDjKjrVU2FGjtLg2l5bXfMQsCQOqnY/xVLVoVqz0CGeKeFJomDxOMq461RODS5KU+aHAeVV0NYoc0BhxDk1CClsnblShRyEhudBEZyAAYA59aJBADxe1AgeKYQHFSggSojoyOoZG2ZWGQfkGmXALIp8P6fcQSItpCilGVWEag5IwMbVv0unnk5t7TLn1Chx7PHdU0yWKRiF9SHBHxTqZuiMWcLk54aWTLKLyBDwgCkFZPiCsOE7kVBWza+E7SOw0O4uZslbhyyKdxgekED3NdvQ4d2Opc2zia7PU+PSJcdxbOxXzAD0O/8ANPn6Rjkvx/FmXF1OS88odCZHEpDgc+E5xXEz9Ny41bVr+HWxa/HPhOglauezXYanekoYU7b9alUSzlO1AIo3NFAFbYVYot8IrckuWNnI5nA962Y+n5peq/6ZsmuxR9nG4t4lLY8xhjORgDPtXX03TIw5l+T/AMOXn6i5cR4CGoObiJcBUV0yB2JrobElSMUcrcrZgfEeni31C5jYbrIwPwTkfwa8xlW2bR6/DLdFMzfkFXPDyztS2XE1OJQFAzmmSK5F14e0G61K+Ea5SBd55Mfav+p6VpwYHOX8MufOoR/prdanjEtvploPRFgMq78tlXavS4YbVfo8xqMluvZXZKPIXBHAcNn26VrZjJFt56Is7BlMp9B3AwOxquTTHVrks7eKaQlSu4+5j6QB71ytV0/Fk58P+HS02tyw48r+htGUfGQw6MDkV53V6R4pU/Ho7unzrJG/Ya4POstF1iHaqwoEMSdh8CjFWwPgsotPglt3SRyJjg5HQZ5V6TR6TtVJ/scbUZu7cfRDudEuxN/YuJUbDYPpII5101NHPnppLxyRotG1JlAeE5b1MSRjOfmjviVdifwJtD1LzSSqgEA/cNqG9DrTzD8ReFl1SJLheFL4IFk/K4Hc9x0Nc3VaVZOV5O1pdS8ap+DEP4O1WJyDbuTy2Gf2IrlvT5E/DOmtTBryWmleBLt2V7r+xHM53b9BWvDo5vzwjLm1kV45NtY6bbWVuLe3ThTmx6se5NdTHBQVI5WSTm7YUWlWST+ekKiYf1gb1a8jaopjjjd0ONaW/wDaL5SnzGzJsPUT3ob2HYvg5IsEMAygKpsiYGM9MUqtsaVRRAYszF23J5mr0ZG7GZ4wyHA35j5rJrcHdxte14NWkzbJr4yIjivKI9FRzNmqkMOWWPPyenL5rpdKxKWW36Rg1+Soceyys5R5z5PNdvnIr0kkcfE+SauTjuppGaCQ221IhhmU4c0y8AsbLHGBvRSIwDw9FOe1EA01xIuwjx70LICl1GCWkYZ7DenSbElNIc/2lHnZCfnaj2mVPOiHJqkgeX0gYAIYdNj/AKVYsS4K3mfJHOoOxeSdiUjwiL3cjfFNsS8FbyN+SDc6wwYohAYfexPpQHue9WxxoolkfobS7kbcOzD82Dv++KZxApMejfOe9eN6lg7eV/Hyep0ObuY19XATbVzEzc0R7yd4bcyqd0ZW27A716Po2Phy+8HC6rk/JR+E2OYSbqfS4yK7Xs5qZKgu51UYc/rvy+aDghlkl9HzqVztuPfYUFiQ3fkC1/ck5yP2qdtB70gGvLk/1Y+AKDgid1iG5uGH3kfsKbagdyQJLt97Fh7k0aEbZxQY9NEURXZdiKhGRLhwJGycB0H/AKt/+0wKKZ7uSZuGPdk4sL/xM5BY/OP2FNZVIm6VoVzMQ7jhjyWWVx6nJ5sqn/MaEsyRZDTykXY8PQgZeVgf0/x51U9Q/hetKl7IN3pzWhDBuOM7Z5EGuN1iO6Cl7TOn02LjJr0xpztjpXm0dkLUdJu/wylF8xHQcYHMcQ7V67p0VDGkea6hGUsja8FJYak8IaOUE8AbB7Fd8frXTlE50J0aC2nikjEiMCjjiXfoRmgy1McDGoiMLiHU1CHFk/Oo/UVCWNvcQpuZB8DegosjmgDqEGNst7020RzQxLqcqDMce3UnJ/wo7QPIQn1suf7wpwglwBjYVKQu5lPq9/I9slxBMWUkqCDzDKR/mxRdBjdlv4AtY7nSzeyAn8TKxXqTGp2A+ay5MnNG7Dh9s3CRsB0T2G5x81VZpaBaSJeRBPVmO1NTK3JEDU147RyDxYwSR81k6hG8LNOklU0VunwC5u0Q/aDxP8CvO6PBvyJHVz5NkbNDKM7javVJHGZlte0uAO1xEojmAJHCNnOOTf61dDK0qZly6dPleTPeH7vzoXhfK8DejPZt8fodq0p/6YJRos91yCTt1zRFF4j+YnNQghfHKoQJZVx6hUIFxjmvKoAakuDGCRy7UQlNdamT5gnhw5BEbRjPFmhLgeMb8FVBaamVkDQMLWQZQnbDewO5qnuJM1LC2rrwav6V3F3aaPd2V3GVa2uWW14tiY2Af5AUnFUONs0qdRs1000k2xbCflGwq2MUjNPI2NGNe+e3amsqGZI9iMnDbH3pcsFODi/ZbiyOEkz/2Q==" class="rounded-circle" alt="user"></span>
                    <span><span class="account-user-name">Matheus</span><span class="account-position">Administrador</span></span>
                </button>

                <div class="dropdown-menu-animated topbar-dropdown-menu profile-dropdown dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    @php $profileEdit = route('user-edit', ['id' => Auth::user()->id]) @endphp
                    <a class="dropdown-item" href="{{ $profileEdit }}">
                        <span> Minha Conta</span>
                    </a>

                    <a class="dropdown-item"  href="{{ url('/admin') }}">
                        <span>Configuração</span>
                    </a>

                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Sair</a>
                    <form id="logout-form"   action="{{ route('logout') }}" method="POST" style="display: none;"> @csrf </form>
                </div>
            </div>
            @endauth
        </div>
    </nav>
    <!-- End Navbar -->

    <main class="container">
        @yield('content')
    </main>

    <div class="modal fade" data-url="" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="altera-senha-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form action="" method="post">
                    @csrf
                    <div class="modal-header bg-primary text-white">
                        <h4 class="modal-title" id="myModalLabel">Alterar Senha</h4>
                    </div>
                    <div class="modal-header">
                        <div class="container">
                            <div class="row justify-content-md-center">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="senha-atual">Senha Atual</label>
                                        <input required class="form-control" type="password" name="senhaAtual" id="senha-atual">
                                    </div>
                                    <div class="form-group">
                                        <label for="senha-nova">Senha Nova</label>
                                        <input required class="form-control" type="password" name="senhaNova" id="senha-nova">
                                    </div>
                                    <div class="form-group">
                                        <label for="confirmar-senha">Repetir Nova Senha</label>
                                        <input required class="form-control" type="password" name="confirmarSenha" id="confirmar-senha">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary p-2">Alterar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img_fake').attr('src', e.target.result);
                $('#image_content_base64').val(e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".icon-close").on('click', function () {
        $('#img_fake').attr('src',"");
        $('#image_content_base64').val("");
    });
</script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</html>
