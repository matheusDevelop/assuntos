<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2>Resultados para: {{$request->s}} </h2>
            <div class="mainconteudo" id="results-wrapper">
                @foreach($subjects as $subject)
                    <div class="col-md-12 conteudosingle">
                        <div class="row">
                            <div class="col-md-9 col-lg-10" data-toggle="modal" data-target="#modalVisualizar" onclick="" >
                                <img class="conteudoimgthumb" src="{{$subject->image}}" >
                                <div class="infoconteudo" style="">
                                    <h3 class="titleconteudo__bold" style="font-family: 'Nunito', sans-serif;">
                                        {{$subject->title}}
                                    </h3>
                                    <p class="paragraphconteudo">
                                        {{$subject->description}}
                                    </p>

                                    <div class="detailsconteudo" style="margin: 0 0px -90px 0px;">
                                        <label>

                                        </label>
                                        <label>

                                        </label>
                                        <label>
                                            Matheus Oliveira
                                        </label>
                                        <label>
                                            Widgets: 3
                                        </label>
                                        {{--<label>--}}
                                        {{--Data de cadastro: 07/07/2019--}}
                                        {{--</label>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-lg-2 opcoesconteudo">
                                @php $subjectEdit = route('subject-edit', ['id' => $subject->id]) @endphp
                                <button onclick="window.location.href='{{$subjectEdit}}'" class="btn btn-link btn-block"><i class="icon-editar"></i> Editar</button>

                                @php $subjectDelete = route('subject-delete', ['id' => $subject->id]) @endphp
                                <form action="{{$subjectDelete}}" method="post">
                                    @csrf
                                    <button class="btn btn-link btn-block"><i class="icon-apagar"></i> excluir</button>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>