@extends('layouts.app')

@section('title', 'Tudo Sobre')

@section('content')
    @if(session('message'))
        @alert(['type' => session('type')])
        {{ session('message') }}
        @endalert
    @endif

    <div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Título do modal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary">Salvar mudanças</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="page-title-box mt-md-2 mb-md-2">
                <div class="page-title-right">
                    <a href="{{route('subject-create')}}"class="btn btn-primary p-2">
                        Novo Assunto
                    </a>
                </div>
                <h4 class="page-title">Assuntos</h4>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach($subjects as $subject)
            <div class="col-sm-12 col-md-12 mb-2">
                <div class="media align-items-center shadow-sm border bg-white rounded">
                    <img class="align-self-start d-none d-md-block" src="{{$subject->image}}" alt="{{$subject->title}}" title="{{$subject->title}}" style="width: 115px;height: 115px;">

                    <div class="media-body align-items-center p-3">
                        <h5 class="mt-0">{{$subject->title}}</h5>
                        <p class="m-0 d-none d-md-block">{{$subject->description}}</p>
                    </div>

                    <div class="border-left d-flex flex-column">
                        @php $subjectEdit = route('subject-edit', ['id' => $subject->id]) @endphp
                        <a href="{{$subjectEdit}}" class="btn btn-link" role="button">
                            Editar
                        </a>
                        <div class="border-bottom"></div>
                        <a href="{{$subjectEdit}}" class="btn btn-link" role="button" data-toggle="modal" data-target="#modalExemplo">Apagar</a>
                        <div class="border-bottom"></div>
                        <a href="{{$subjectEdit}}" class="btn btn-link" role="button" data-toggle="modal" data-target="#modalExemplo">Incorporar</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
