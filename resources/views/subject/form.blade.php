@extends('layouts.app')
@section('title', 'Tudo Sobre')
@section('content')

    @isset($success)
        @alert(['type' => $success])
        {{$message}}
        @endalert
    @endisset


    <div class="row">
        <div class="col">
            <div class="page-title-box mt-md-2 mb-md-2">
                <div class="page-title-right">
                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            <li class="breadcrumb-item"><a href="/">Assuntos</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Editar</li>
                        </ol>
                    </nav>
                </div>
                <h4 class="page-title">{{ !empty($subject->title) ? 'Editar' : 'Assunto' }}</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <!-- Card -->
            <div class="card">
                <div class="card-header bg-white p-4">
                    <h4 class="header-title m-0">{{ !empty($subject->title) ? '' . $subject->title : 'Novo Assunto' }}</h4>
                </div>

                <div class="card-body">
                    @php $actionRoute =  empty($subject) ? route('subject-create') : route('subject-update', ['id' => $subject->id])   @endphp
                    <form  action="{{$actionRoute}}"  method="post" >
                        @csrf

                        <div class                ="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <div class="dropzone">
                                        <div class="dz-message needsclick" tabindex="0">
                                            <input multiple="" type="file" autocomplete="off" tabindex="-1" style="display: none;">
                                            <i class="h1 text-muted dripicons-cloud-upload"></i>
                                            <h3>Selecione uma foto</h3>
                                            <span class="text-muted font-13">(Essa foto será utilizada para indexação no google)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-9">
                                <div class="form-group">
                                    <label for="firstname">Nome</label>
                                    <input class="form-control @error('name') is-invalid @enderror" value="{{ !empty($subject->name) ? $subject->name : old('name') }}" name="name">
                                    <div class="invalid-feedback"> @error('name') {{$message}}@enderror </div>
                                </div>

                                <div class="form-group">
                                    <label>Titulo</label>
                                    <input class="form-control @error('title') is-invalid @enderror"  value="{{ !empty($subject->title) ? $subject->title : old('title') }}" name="title">
                                    <div class="invalid-feedback"> @error('title') {{$message}}@enderror </div>
                                </div>

                                <div class="form-group">
                                    <label>Slug</label>
                                    <input class="form-control @error('slug') is-invalid @enderror"  value="{{ !empty($subject->slug) ? $subject->slug : old('slug') }}" name="slug">
                                    <div class="invalid-feedback"> @error('slug') {{$message}}@enderror </div>
                                </div>

                                <div class="form-group">
                                    <label>Descrição</label>
                                    <textarea class="form-control @error('description') is-invalid @enderror" rows="5" name="description">{{ !empty($subject->description) ? $subject->description : old('description') }}</textarea>
                                    <div class="invalid-feedback"> @error('description') {{$message}}@enderror </div>
                                </div>
                            </div>

                        </div>

                        <footer class="card-footer  p-4">
                            <button class="btn btn-primary p-2 mr-2" type="submit">Finalizar edição</button>
                            <a class="btn btn-link" href="{{route('subjects')}}">Cancelar</a>
                        </footer>

                    </form>
                </div>
            </div>
            <!-- End Card -->
        </div>
    </div>
@endsection
