@extends('layouts.app')

@section('title', 'Tudo Sobre')

@section('content')
	@if(session('message'))
		@alert(['type' => session('type')])
		{{ session('message') }}
		@endalert
@endif

<div class="container-fluid">
	<div class="row">
		<div class="col">
			<div class="page-title-box">
				<div class="page-title-right">
					<nav class="" aria-label="breadcrumb">
						<ol class="breadcrumb bg-transparent">
							<li class="breadcrumb-item"><a href="/pages/starter">Assuntos</a></li>
							<li class="active breadcrumb-item" aria-current="page">Perfil</li>
						</ol>
					</nav>
				</div>
				<h4 class="page-title">Minha Conta</h4></div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="profile-user-box card-body">
					<div class="row">
						<div class="col-sm-8 offset-sm-2">
							<span class="profile__img">
								<img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBAQFBAYFBQYJBgUGCQsIBgYICwwKCgsKCgwQDAwMDAwMEAwODxAPDgwTExQUExMcGxsbHB8fHx8fHx8fHx//2wBDAQcHBw0MDRgQEBgaFREVGh8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx//wAARCACAAIADAREAAhEBAxEB/8QAHAAAAQQDAQAAAAAAAAAAAAAAAQACBQYDBAcI/8QAPBAAAgEDAwEFBQUFCAMAAAAAAQIDAAQRBRIhBhMiMUFhBxQyUXEjgZGhsRUzQlJyCCQlYoKiwdFDg7L/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAQIDBAUG/8QAJxEAAgIBBAICAgIDAAAAAAAAAAECEQMEEiExQVFhcQUTFIEiMlL/2gAMAwEAAhEDEQA/APRGKkoLFAKgARQANAA0AKAXFACgAaAVACgGkUA00ADQDSKAkcCgFtoAYoBtAYrieC3hknnkWKCJS8srkKqqvJZieABQHKuofbzYRXLQdO2i6gieN5OXiibH8g27iPU4qrnRpHFfZWV/tH6vbyD37T7EoT8MckmcD173/wA1CkyXjRdumPbl0brbJFM506ZgP3zqY9x4xvGPzFWTKuB0QEEAggg8gjkEVJQNAA0AKAFAAjzoBtANIoCRoAZoBH50A00B589rXtLkv9dn0GzZ59NsyVeK35Esi/E0pPG1W4UffWcrZvjSX2c7ZtRuY9osJZ7dj9oIW7Pj1fwP3VWkjTkV1pmkwwrLJYLEcZMM5WYn/UjE/pUX8hxXogbjVNILrDHpwsyOO0idmTnjkN5VemZuvR1v2G+114tTj6O1qYvbsRFpNy5HcPOI3YnwbgJ8q0MpI9CUKioAEUADQAoBpoBpFAb9CRUAqEEH1tfzWHSmq3cXEsdu+xhkEFht3DHyzmol0WiuUeSoImv7qZ4UjnUMctKWHh6AjA+Zzk1k3R0pWy0aB0t1P1BEI9Jx2UZw15HBH2AI4Kb5N7N91ZuSRoo/Jdumv7PVoZDc69M8+eTFGOyBPzwCRTc2Q1FE/f8Ase6GghIisMEjHLFs/jmqOTLxr0cX9pPs1h0R01XSHZBHhjET3kZTwVPpitsWXwzLNiXaPSHs66rTqno/TtX8LiSPs7xP5Z4+7J+J5H1rc42iy0AKAFACgGkUA00Bv0JAaAVAaGu6ZHqmjX2nSAMl5BJCQfDvKQPzownR5g6I6TvL/W00+WYWtlOzCdQMybVO1kB8uR5VzTZ240eltFsLHTbKGxs4litrcBI0XgYFUREiWMgIx5VpZSiNu4hJnI+lYyVmsXwc09odjF3llCPE64K5GVPzx41EFTNG00R/sH1/TNOTU+nrmXsbh78e6IfhYvH8IJ8zs4FdikcUsbdteDtFXMRGgGmgAaAFAN86A3qEioAUAKA45p9kmm+0TU9PdApgJuLcnxMUx3DH41zZI0zsxytF4bqnSLJSZ2kkcc7IUaQ/Tu+dZqi+1s3tN6s0W+ZVhfbK/wAMMhAf71BNTvRDxyKr7QuqNYFwND0WCdr6UK0r24C9mrHALSsMIDg81DfJaEFVlGOn9RM6W02npEshKSPLN2878fF4+fpTheS6trlUYdB0htK61EMxRVuB3pWzuWQbTGyevFTKVqxCNWd6spzPZwT5BMsaOSPAllB4rrXR5klTZmqSAUAKABoBpA8aA3qEgoAGgFQFE6m6edtZi1lCz3ULSJNIQoAtmAwmRgnDHI8a5Jt2zuhTijSuejtSvRsW/ngsyM9hasIyxbkkueazTdmlqiV0Xo3S9Ht90FuizEBRI32kmc53M5/iz51MiE/AOoI2ttVt9SVSwkRYplxndtJ8h9arIvjpqje/wlrUXNp2Q3ruDoB5+eaWiKfk5tqOmXusdd2EVkkbuvaST7mCjYqFcgnz79awjaopKai7Z2Ozt/d7WG3B3dkipu8M7RiuqKpUcEnbszGpKgNBQ2gBQAOKA3aEgNAKgAaAj9Ut+0hlQttjnQoxxkBsYB/SscsebN8UuKIbQNQL6eiy/HHlXzwcg4rns6fI6fUbm5uZ44SgithiNScdpKBnacZO0fP51VuyySSKt1nqnWjaRDbwwWFhO7Za+Nx2xiXz2wlUdmx6VbjyTCLt0V3pqXV572Dp6yMxh05Fe+1K7iaMuZDkRRA4ZixJPIwBVaXZdsnOm4kX2jW0UWT2dvcs/n3VyufzWujAcmofB0+ug5BZ4oAVBIOakqKgGkUBu0JEaAFAKgACR4UIKXcyR6V1JcW02FhvSbi1yeG3fvFHqr/ka5c0adnZilaox33SNhqss8/vNzazuysghmZFJzuYOqkblJ8ayRuslGQRX+nWpijihgxwZ4I41bGMcyPuaptpFv8ACTttkLNK+iw3mqXb7VQNIXJJLPtOPHlj61VKyZyT66NT2M3A1e/1jXJR9oFitbYeQjyWkYf1Oo/CuzGqR5+aVnVK0MQGgBUAFSAUADQG5QkRoAUAqAFAV/rfp+31vQpIHZoZ4WWW2uoziSJwcblP0PNVn0Xxvk5JqWt9edJXMA1VPeLWAERamu4rID5S4+A/diubYn0de72ZNV9uOl3WltC+2GdgVkjwT4jhgfrUPHImM4LkovU3tC1Dq9Y9Os9yWynMsvO0KPT5mtIY9vLKTybuEdC9kOtaZofbpqN1FZWc0Kqk0zbE3o/dBY8DIY+NaQfJjlVI7DZajYX8Xa2NzFdxfzwSLIPxQmtWqOdNPoz1BIjQAoAUADQG5mhIKAVACgNXUtU0/TLVrq/nS3hX+JjyT8lUcsfQVaMHJ0ik8kYq2ysW/WUmuTzWunQCOzVCWmm/euPDuoOF5+ZNTqcTxxV9sjS5llk66RLiC3vLPsZ41kjdcMjjINcJ3lF6g9jnRt6zyC393kPJMZwM/Sp3NE8eUVA+zey0657KxLSIeCxGPwpuLJIWoaRGsRtiNxOVWMDOcDkVaw0VTTrKfTr2Q2Ur2sqt3ZImaNsD1Ug19FijcFfo+TzT25HXst2n9edZWbADU5Zgv8E4WVePVwW/OktLB+BHV5F5LRpftjmB2anYLJj4pbZip+9GyPzrmnofTOqH5D/pFq032i9KX7CMXZtpT/47lTHz/Vyv51yz004+DqhqoS8ljV0dA6MGRuVZSCCPQisDosVCTcoBUFmO4ngt4WnuJFihjGXkchVA9SalJvohySOe9Se1u2hLW2hRieUd03koIjB/yIcFvqa7sOib5kefm16XETnN/qepancm5vbh7mdjlXY5x/SBwo9BXowxqKpI8yeSUnbZJaFrNzpV5HdRYYpxIv8ACyn4gfrVM+BZI0y+DO8UtyOoaH1Lo2pRDsZ1im87eUhHH0zw33V4OXSTx9o+iw6zHkXD59Ew9uJE8NynzHNc9HSpEbPp1rGzTTMkSKCSzkKPxNFBvoPIl2c/1vWNJt7VRZ4mvtzt2gH2aA8ct5n6V36bQSk7nwjz9X+SjFNQ5ZSmQb9xHxHPrg8nNe4kfPNmRUAzuXcPlggigFsUZIGCePEDxoLGSwEr5HPgf+8UoI3+nurNb6euUMEpktMjtrViSjLnnAPwn5EVz5tOpL5OrDqZQ+juOlarZapp8GoWT9pbXC7kPmPIqw8iDwa8eUWnTPbjJSVomaqWKb1D7S9I09mttPxf3g4O0/ZKfVh8X0X8a6sWklLvhHHm1kY8LlnMtb1zXNem36hct2YOY4E7sa/RRXp48EYdHlZdRKfZHCxUHJ73zOf1rYxH+5jBAJXPIwKgCS2mjOd2fI/OpBlXtWGfMeJqAO94u42PZyug8SFZhj1xkVDin4LKcl0xrPNNlnkZgDwxJP6nNFFLohyb7ZhlU7c88nj6ffViBhYBTkZPiDny9KgAPJBfnxx5/dnNAJsY4yR8vH5UA8tGto03yZQOfDJxUsGpf4+wyBulRkU4wN25Rn8DUPgmKsuvsd17bfXmiu32UyC7tFPkVwsgH1XafurzNZDqR6uhydxZ0LrzVhp/T8qBsTXh93jwcHDDLnP9NYabHun9HRqsmyD+Ti/uqrKCjd0nC+hx4V7R4PZkQ7CdxyR5CpIHlhs3bfHzxmlCwoRuG4h+B8sAfjQCIXBJyTn7vzoSMclSSBkfM8YNCLMaQyTMREhc+QA8h9KWkSk2bUdhqGAFtZufHETk/kKo5L2XUJemObR9YkUH3OUKOe8No+pLYqv7oe0W/TN+GMfS9SwS0OEBzjcgH1+Kn7oeyf4+T0NttHv7knsYwzpye/GMD1y1VeaC8krTzfgx3WlahaAvOiovGftEYf7WNTHNB+Q9PkXaI64bOm3a5G5UMmfMlDnH5VrfBglyR+rzgtp3Z8l1kK+XeKbV/wBzCom+i2NdjtL1WXRNYs9UgBY2cwLAfxRD7N1/1LmsM0NyaNsM9skzp3tP1kXGsCzRu5ZJsPy7R+835YFZaHHUb9nR+QyW9vooyT87TyB3kbx5U+vpXe0eYjJFNvQ4OG3EgHPz/SlE2bCglB4c4HxYA/HwqG0uSUm3SN+2sdHjSRtR1SGCWOPtpYEKmRYsgbsMQ23vDlVauPJrUuErO7HoG/8AZ0SE79G6bfy2l6sscME8lnNqcnaSWq3UcPbdiWDwd5k8O7jPFcz1eRnVHRY18mzodtquoPbXen2ttbQx2kF4tjKgWd/elkZEaSNMrlY/E5wT6VlLLJrls2jhiukiVOuhLvT7lUDaO88ou1d5TIts9vbzQycPtBj94Jk45UH5VlZtRq6tIkmralG6utjHd2UNotqivcGMTta3QQuH3F5uPTHFEwzclsv2beaXd2NtcdjFBeT3WnX0aNPPHE8O/PB76RuzxYPp50sgw2emLeBtTOnnWNPhaZIxBcGB07G5lcSqgMaTdpE0eMtnjFTZFGbQbbTdW1eWG47SW1CXEunbZJISd9wJd47Modyw3EIXPwiobslIrusXl8+j67cXEzTo1tGly7eCPA8lslwv8oeS02y44O/PlUxdMhlPkA27iCUdSHU4HxDn9a9qDtHg5Y02VyG8DtorucrFDPI3/qIH/FLui+2t32PuMiyw2d+BuPqef+6q0Quy26lfyXdzNczHMksjSufHvMcn9avjjtVFMs9ztkZG537fHxB8/EYrZoyNizLMAuO94A8fn5UDN+RP7qwZeI0YlfnxkYwKpPpovjdNMjNMknlt9S0vRp7bXJ9cgup71Vsm/aMJaLtow92fjHaqsapnn5V4k40z6DHK1ZZNX6X6ut+ldW6fvolubLUQl/ZX1wVWT3qdEeWOdRkh4pEwG+VIRTT9kydFg0B+pJeoJpLOSCGSS0hgd7pJCE7LcQyRx4U47Q4BYeFWyQpERdlvs+j7C2WJDIZYIlVDE6gh1FkbJw5/zphj6isDQxjozTY9OtbSO4uU91iWGO6Do02UuBdLKzMjBpO1XOSPM8UIJSw0eOB4J5rq5vbm3MhinuXUsolUK6gRrGuDtHlQkirzo/SFt1trS0C2/aSyPbe8TwxkzndJkISCGP8ACRjHhUkCvOn47mBoZrK1Ze0EiAFk7wQRbgyIrIezRV7vlxUAw/sfs7NoHs7RLN7cWs0KHu+7jJMWDGAVG5vxqUQcH1m9SG0u548qkcbvHzwOO7XrwdQ/o8WavJ/ZAW0eyTR4fH+6SNJ/qZWI+81auUvgN8SfySN4X7Paw+JsD6Kp/wCWqZGcT//Z" alt="" class="rounded-circle img-thumbnail" style="height: 100px;">
							</span>
							<div class="media profile__media ">
								<div class="media-body">
									<h4 class="mt-1 mb-1">matheus</h4>
									<p class="font-13 text-dark-50">Administrador</p>
								</div>
							</div>

							<div class="form-inline">
								<div class="col-sm-6">
									<label for="text" class="profile__form--label">Nome de usuário</label>
									<input name="text" id="text" placeholder="matheus" type="text" class="form-control profile__form">
								</div>
								<div class="col-sm-6">
									<label for="text" class="profile__form--label">Nome</label>
									<input name="text" id="text" placeholder="Matheus Oliveira" type="text" class="form-control profile__form">
								</div>
								<div class="col-sm-6">
									<label for="text" class="profile__form--label">Senha</label>
									<input name="text" id="text" placeholder="••••••••" type="password" class="form-control profile__form">
								</div>
								<div class="col-sm-6">
									<label for="text" class="profile__form--label">Confirmar senha</label>
									<input name="text" id="text" placeholder="" type="password" class="form-control profile__form">
								</div>
								<div class="col-12">
									<label for="text" class="profile__form--label">E-mail</label>
									<input name="text" id="text" placeholder="matheus@verdesmares.com.br" type="text" class="form-control profile__form">
								</div>
							</div>
						</div>

					</div>
				</div>
				<footer class="card-footer  p-4">
					<button class="btn btn-primary p-2 mr-2" type="submit">Finalizar edição</button>
					<a class="btn btn-link" href="http://127.0.0.1:8000/admin/assuntos">Cancelar</a>
				</footer>
			</div>
		</div>
	</div>
</div>

@endsection
