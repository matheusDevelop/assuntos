var ajax;
var title;
var description;
var image;

try{
    ajax = new XMLHttpRequest();
}catch(ee){
    try{
        ajax = new ActiveXObject("Msxml2.XMLHTTP");
    }catch(e){
        try{
            ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }catch(E){
            ajax = false;
        }
    }
}

window.subject = function(slug){
    ajax.open("GET", "http://127.0.0.1:8000/api/subject/" + slug, true);
    ajax.send();

    ajax.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {

            var response = JSON.parse(ajax.responseText);

            if(response.success){
                title =  response.response.suject.title;
                description = response.response.suject.description;
                image  = response.response.suject.image;
                card   = response.response.widgets.info;

                document.title = title;
                document.getElementById("oraculo").innerHTML = card;
                document.getElementsByTagName('meta')["description"].content = description;
            }
        }
    }
};


window.subjectBlade = function () {

};