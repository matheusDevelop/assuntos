<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    private $path = 'profile';

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path.'/form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email'  => 'required|unique:users',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('admin'))
                ->withErrors($validator)
                ->withInput();
        }

        $users = $this->_save($request);

        $request->session()->flash('message', $users->name . ' criado com sucesso!');
        $request->session()->flash('type', 'success' );

        return redirect(route('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        return view($this->path.'/form', ['user' => User::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name'   => 'required',
            'email'  => [
               'required',
                Rule::unique('users')->ignore($id)
            ],
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect(route('subjects'))
                ->withErrors($validator)
                ->withInput();
        }

        $user = $this->_save($request, $id);

        $request->session()->flash('message', $user->name . ' alterado com sucesso!');
        $request->session()->flash('type', 'success' );

        return redirect(route('admin'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::find($id);
        $user->delete();

        $request->session()->flash('message', $user->name . ' excluido com sucesso!');
        $request->session()->flash('type', 'success' );

        return redirect(route('admin'));
    }

    public function _save($request, $id = null){

        $user = ($id == null) ? new User() : User::find($id);
        $user->name = $request->name;
        $user->password = Hash::make($request->password);
        $user->email  = $request->email;
        $user->save();

        return $user;
    }
}
