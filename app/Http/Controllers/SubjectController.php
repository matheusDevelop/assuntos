<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class SubjectController extends Controller
{
    private $path = 'subject';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjecs =  Subject::orderBy('title')->paginate(10);
        return view($this->path.'/index', ['subjects' => $subjecs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path.'/form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|unique:subjects',
            'slug' => 'required|unique:subjects',
            'title' => 'required',
            'description'  => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('subject-create'))
                ->withErrors($validator)
                ->withInput();
        }

        $subject = $this->_save($request);

        $request->session()->flash('message', $subject->name . ' cadastrado com sucesso!');
        $request->session()->flash('type', 'success' );

        return redirect(route('subjects'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view($this->path.'/form', ['subject' => Subject::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view($this->path.'/form', ['subject' => Subject::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name' => [
                'required',
                 Rule::unique('subjects')->ignore($id),
            ],
            'slug' => [
                'required',
                 Rule::unique('subjects')->ignore($id),
            ],
            'title' => 'required',
            'description'  => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('subject-edit'))
                ->withErrors($validator)
                ->withInput();
        }

        $subject = $this->_save($request, $id);

        $request->session()->flash('message', $subject->name . ' editado com sucesso!');
        $request->session()->flash('type', 'success' );

        return redirect(route('subjects'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $subject = Subject::find($id);
        $subject->delete();

        $request->session()->flash('message', $subject->name . ' apagado com sucesso!');
        $request->session()->flash('type', 'success' );

        return redirect(route('subjects'));
    }

    public function _save($request, $id = null){

        $subject = ($id == null) ? new Subject() : Subject::find($id);

        $subject->name   = $request->name;
        $subject->slug   = $request->slug;
        $subject->title  = $request->title;
        $subject->description = $request->description;
        $subject->image = $request->image;
        $subject->save();

        return $subject;
    }


    public function search(Request $request){

        $subjects = Subject::search($request->s);

        return view($this->path.'/search-result', ['subjects' => $subjects, 'request' => $request]);
    }
}
