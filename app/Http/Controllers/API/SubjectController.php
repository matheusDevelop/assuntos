<?php

namespace App\Http\Controllers\API;

use App\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubjectController extends BaseApiController
{
   public function show($slug){

       $subject = Subject::getSubjectBySlug($slug);

       if(is_null($subject))
           return $this->sendError('Not found');

       $data['suject'] = $subject->toArray();
       $data['widgets']['info'] = $this->widgetInfo($subject->title, $subject->description, $subject->image);

       return $this->sendResponse($data, 'Products retrieved successfully.');
   }

   public function subjectJs($slug){

       $subject = Subject::getSubjectBySlug($slug);

       if(is_null($subject))
           return $this->sendError('Not found');


       return view('subject/subject-js', ['subject' => $subject, 'widget' => $this->widgetInfo($subject->title, $subject->description, $subject->image)]);
   }


    public function widgetInfo($title, $description, $image){
       $card =  '<div class="c-blog-profile u-display-flex u-f-direction-column u-justify-content-center u-text-align-center">'.
                '    <div class="c-blog-profile__media">'.
                '        <a class="c-blog-profile__link" title="title">'.
                '            <div class="c-blog-profile__photo">'.
                '                <div class="avatar avatar--huge">'.
                '                    <figure class="media">'.
                '                        <img src="'.$image.'"  alt="'.$title.'">'.
                '                    </figure>'.
                '                </div>'.
                '            </div>'.
                '        </a>'.
                '    </div>'.
                '    <div class="c-blog-profile__inner">'.
                '        <a class="c-blog-profile__link" title="title">'.
                '            <h1 class="c-blog-profile__name text--heading-2 u-margin-top-2">'.$title.'</h1>'.
                '        </a>'.
                '        <div class="c-blog-profile__intro">'.$description.'</div>'.
                '    </div>'.
                '</div>';

       return $card;
   }
}
