<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = ['name', 'slug', 'title', 'description', 'image'];

    static public function search($s){
        return self::where('title', 'like',  "%$s%")
                   ->orwhere('name', 'like', "%$s%")
                   ->orwhere('slug', 'like', "%$s%")->get();
    }

    static public function getSubjectBySlug($slug){

        $subject = self::where('slug', $slug)->get();

        if(empty($subject[0]))
            return null;

        return self::find($subject[0]->id);
    }
}
